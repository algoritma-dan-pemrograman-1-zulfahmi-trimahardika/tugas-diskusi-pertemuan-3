#include <iostream>
using namespace std;
int main()
{
	// membuat variabel a, t, dan luas dengan tipe data float
	int a, t, luas;

	// memasukkan nilai bilangan pecahan panjang alas dalam satuan cm
	cout << "Masukkan panjang alas (cm) ";
	cin >> a;
	
	// memasukkan nilai bilangan pecahan panjang tinggi dalam satuan cm
	cout << "Masukkan panjang tingginya (cm) ";
	cin >> t;

	// operator aritmatika untuk menghitung luas segitiga
	luas = a * t / 2;
	
	// menampilkan hasil luas segitiga
	cout << "Luas Segitiganya adalah " << luas << " cm" << endl;
	return 0;
}
